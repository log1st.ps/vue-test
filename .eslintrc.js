module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "airbnb-base",
    "plugin:vue/recommended",
    '@vue/typescript',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    "import/no-unresolved": [0],
    "import/prefer-default-export": [0],
    "import/no-dynamic-require": [0],
    "import/no-extraneous-dependencies": [0],
    "import/no-mutable-exports": [0],
    "import/extensions": [0],
    "global-require": [0],
    "max-classes-per-file": [0],
    "class-methods-use-this": [0],
    "no-sequences": [0],
    "no-async-promise-executor": [0],

    "vue/no-v-html": [0],
    "vue/require-default-prop": [0],
    "vue/prop-name-casing": [0],
    "vue/require-component-is": [0],
    "func-names": [0],
    "no-restricted-globals": [0],
    "no-nested-ternary": [0],
    "no-param-reassign": [0],
    "linebreak-style": [0],
    "vue/max-attributes-per-line": [1, {
      "singleline": 3,
    }],
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
      ],
      env: {
        mocha: true,
      },
    },
  ],
};
