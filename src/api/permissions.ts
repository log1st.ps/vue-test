import fakePermissionsResponse from '@/tmp/fakePermissionsResponse';
import { IPermissionSource } from '@/interfaces/models/IPermission';

const fetchPermissions = async (): Promise<{data: IPermissionSource[]}> => fakePermissionsResponse;

export {
  fetchPermissions,
};
