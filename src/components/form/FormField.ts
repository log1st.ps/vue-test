import {
  Component, Emit, Model, Prop, Vue, Watch,
} from 'vue-property-decorator';

@Component
class FormField extends Vue {
  @Prop()
  for?: string;

  @Model('input')
  value?: any;

  @Prop()
  label?: string;

  @Prop({ default: () => false })
  isDisabled?: boolean;

  @Prop({ default: () => false })
  isReadonly?: boolean;

  id: string = this.for || Math.random().toString(36).substr(7)

  val!: any;

  @Watch('value', {
    immediate: true,
  })
  handleValue(value: any) {
    this.val = value;
  }

  @Watch('val')
  handleVal(val: any) {
    this.setValue(val);
  }

  @Emit('input')
  setValue(value: any) {
    return value;
  }
}

export {
  FormField,
};
