import { IPermission } from '@/interfaces/models/IPermission';

export interface IPermissionsStore {
  permissions: IPermission[];
}

export interface IStore {
  permissions: IPermissionsStore;
}
