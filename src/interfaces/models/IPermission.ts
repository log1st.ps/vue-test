export interface IPermissionSource {
  id: number;
  parentId?: number;
  name: string;
  checked: boolean;
}

export interface IPermission extends IPermissionSource {
  index: number;
  isIndeterminate: boolean;
}

export interface IPermissionTree {
  root?: IPermission[];
  [key: number]: IPermission[]
}
