import Vue from 'vue';
import App from './App.vue';
import store from './store';

import './plugins/layoutDirectives';
import './plugins/helpers';

import './assets/drop.scss';
import './assets/root.scss';

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
