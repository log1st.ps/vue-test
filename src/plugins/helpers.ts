import Vue from 'vue';
import en from '../locales/en';

Vue.use({
  install: (Vue1) => {
    // Примитивная имитация VueI18n
    Vue1.prototype.$t = (target: string) => {
      const split = target.split('.');

      return split
        .reduce<any>((previous, step) => (previous[step]), en);
    };

    Vue1.prototype.$slotExists = function slotExists(slotName: string) {
      return slotName in this.$scopedSlots;
    };
  },
});
