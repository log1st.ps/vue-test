import { Vue as Vue1 } from 'vue-property-decorator';

const pxOrValue = (value: any) => (typeof value === 'number' ? `${value}px` : value);

Vue1.use({
  install: (Vue) => {
    Vue.directive(
      'd-flex', (el, { value }) => {
        const {
          align,
          justify,
          direction,
        } = (value || {}) as any;

        el.style.display = 'flex';
        el.style.alignItems = align;
        el.style.justifyItems = justify;
        el.style.flexDirection = direction;
      },
    );

    Vue.directive('grow', (el, { value }) => {
      el.style.flexGrow = (typeof value === 'undefined' || value) ? '1' : '0';
    });
  },
});
