import Vue from 'vue';

declare module 'vue/types/vue' {
  interface Vue {
    $t: (target: string) => string | object;
    $slotExists: (slotName: string) => boolean;
  }
}
