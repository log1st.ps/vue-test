import Vue from 'vue';
import Vuex from 'vuex';
import { IStore } from '@/interfaces/IStore';
import permissions from '@/store/modules/permissions';

Vue.use(Vuex);

export default new Vuex.Store<IStore>({
  modules: {
    permissions,
  },
  mutations: {
    setPermissionName: (state, { number, value }) => {
      state.permissions.permissions[number].name = value;
    },
  },
});
