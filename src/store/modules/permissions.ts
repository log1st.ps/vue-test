import {
  Action, Module, Mutation, MutationAction, VuexModule,
} from 'vuex-module-decorators';
import { IPermission, IPermissionSource, IPermissionTree } from '@/interfaces/models/IPermission';
import { fetchPermissions } from '@/api/permissions';

@Module({
  name: 'permissions',
  namespaced: true,
})
export default class PermissionsModule extends VuexModule {
  permissions: IPermission[] = [];

  @Mutation
  setPermissions(permissions: []) {
    this.permissions = permissions;
  }

  @Mutation
  setPermissionsValues(values: {[key: number]: boolean}) {
    this.permissions = this.permissions.map((permission, index) => ({
      ...permission,
      checked: (index in values) ? values[index] : permission.checked,
    }));
  }

  @Action({ commit: 'setPermissionsValues' })
  updatePermission(index: number) {
    const permission = this.permissions[index];

    const indexes = this.permissions.reduce<{[key: number]: number}>(
      (previous, cur, curIndex) => ({
        ...previous,
        [cur.id]: curIndex,
      }), {},
    );

    const newValue = (permission.isIndeterminate ? false : (!permission.checked));

    // И родителю, и всем дочерям в любом поколении меняем значение
    const deepChildrenSearch = (id: number, self: {[key: number]: boolean}) => {
      if (id in this.permissionsTree) {
        Object.values(
          this.permissionsTree[id],
        ).forEach(({ id: childId }) => {
          self[indexes[childId]] = newValue;
          deepChildrenSearch(indexes[childId], self);
        });
      }
    };

    const newValues = {
      ...this.permissions.reduce((prev, p, i) => ({
        ...prev,
        [i]: p.checked,
      }), {}),
      [indexes[permission.id]]: newValue,
    };
    deepChildrenSearch(indexes[permission.id], newValues);

    const deepParentSearch = (id: number| undefined, self: {[key: number]: boolean}) => {
      if (id && (indexes[id] in this.permissionsTree)) {
        const generationValue = this.permissionsTree[indexes[id]].map(
          p => (newValues as any)[indexes[p.id]],
        ).filter(Boolean).length > 0;
        if (self[indexes[id]] !== generationValue) {
          self[indexes[id]] = generationValue;
        }
        deepParentSearch(this.permissions[indexes[id]].parentId, self);
      }
    };

    deepParentSearch(permission.parentId, newValues);

    return newValues;
  }

  @Action({ commit: 'setPermissions' })
  async fetchPermissions() : Promise<IPermissionSource[]> {
    return (await fetchPermissions()).data;
  }

  // Собираем дерево наследников
  get permissionsTree(): IPermissionTree {
    const permissions = [
      ...this.permissions.map(item => ({ ...item, isIndeterminate: false })),
    ];
    // Работаем лишь через индексы, чтобы не путаться
    const indexes = permissions.reduce<{[key: number]: number}>((previous, permission, index) => ({
      ...previous,
      [permission.id]: index,
    }), {});

    // Собираем двухуровневое дерево
    const builtTree = permissions.reduce<IPermissionTree>(
      (tree, permission, index) => {
        const parentIdIndex = ((typeof permission.parentId === 'number') && indexes[permission.parentId as number]);
        const parentId : any = typeof parentIdIndex === 'number' ? parentIdIndex : 'root';
        tree[parentId] = tree[parentId] || [];

        permission.index = index;

        tree[parentId].push(permission);

        return tree;
      },
      {},
    );

    const checkedTree: {
      [key: number]: boolean[]
    } = {};

    const deepSearch = (index: number, self: boolean[]) => {
      if (index in builtTree) {
        (builtTree[index] as IPermission[]).forEach(({ index: i, checked }) => {
          self.push(checked);
          deepSearch(i, self);
        });
      }
    };

    // Ищем все значения дочерей в любом поколении для каждого пермишенна,
    // который есть в дереве, как родитель
    permissions.filter(({ index }) => index in builtTree).forEach(({ index }) => {
      checkedTree[index] = [];
      deepSearch(index, checkedTree[index]);
    });


    // Для каждого из пермишеннов ищем, у которого есть true и false в любом поколении дочерей
    // указываем неопределённое состояние
    Object.entries(checkedTree as {[key: number]: boolean[]}).forEach(
      ([key, checks]) => {
        if (
          checks.filter((v, i, s) => s.indexOf(v) === i).length > 1
        ) {
          permissions[+key].isIndeterminate = true;
        }
      },
    );

    return builtTree;
  }
}
