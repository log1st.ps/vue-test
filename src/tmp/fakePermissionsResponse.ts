// eslint-disable-next-line import/no-extraneous-dependencies
import faker from 'faker';
import { IPermissionSource } from '@/interfaces/models/IPermission';

export default new Promise<{data: IPermissionSource[]}>((resolve) => {
  setTimeout(() => {
    const list = [];

    list.push({
      id: 1,
      name: 'Профиль заведения',
      checked: true,
      parentId: undefined,
    });
    list.push({
      id: 2,
      name: 'Текущий баланс',
      checked: true,
      parentId: 1,
    });
    list.push({
      id: 3,
      name: 'Данные заведения',
      checked: true,
      parentId: 1,
    });
    list.push({
      id: 4,
      name: 'Настройки заведения',
      checked: true,
      parentId: 1,
    });
    list.push({
      id: 5,
      name: 'Услуги',
      checked: true,
      parentId: undefined,
    });
    list.push({
      id: 6,
      name: 'График',
      checked: true,
      parentId: undefined,
    });
    list.push({
      id: 7,
      name: 'Создание, редактирование',
      checked: true,
      parentId: 6,
    });
    list.push({
      id: 8,
      name: 'Меню',
      checked: false,
      parentId: undefined,
    });

    resolve({
      data: list,
    });
  });
});
