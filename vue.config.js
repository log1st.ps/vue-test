module.exports = {
  chainWebpack: (config) => {
    config
      .module
      .rule('svg')
      .exclude
      .add(/\.inline/);

    config
      .module
      .rule('inline-svg')
      .test(/\.inline\.svg/)
      .use('svg-to-vue-loader')
      .loader('svg-to-vue-component/loader')
      .end()
      .use('svg-loader')
      .before('svg-to-vue-loader')
      .loader('vue-loader')
      .end();
  },
};
